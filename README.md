pedal_arcade
=============
<img src="doc/PedalArcade01.jpg" width="712pix" />

Code and docs for the mechatronic parts of Pedal Arcade, an art installation by Rachel Siegel.


https://www.rachelsiegel.net/projects/pedal-arcade

Note: the tachometer design and slightly-modified AVR C code is thanks to [Dave Hylands](http://www.davehylands.com/Electronics/Tachometer/), since moved to https://github.com/dhylands.

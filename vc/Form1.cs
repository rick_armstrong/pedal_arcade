//	Copyright (c) 2014, Rick Armstrong
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//			modification, are permitted provided that the following conditions are met:
//
//	* Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer.
//
//	* Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution.
//
//	* Neither the name of the copyright holder nor the names of its
//	contributors may be used to endorse or promote products derived from
//	this software without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
//	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//			IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
//			FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
//			DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
//			SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
//			CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
//	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO.Ports;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace PedalArcade
{
    public partial class Form1 : Form
    {
        bool Sim = Properties.Settings.Default.SimulationMode;
        SerialPort SP = new SerialPort();

        public Form1()
        {
            InitializeComponent();
            SP.PortName = Properties.Settings.Default.Port;
            SP.BaudRate = 38400;
            SP.DataBits = 8;
            SP.Parity = Parity.None;
            SP.StopBits = StopBits.One;
            SP.ReadTimeout = 10;
            if (!Sim)
            {
                SP.Open();
            }

            Player.settings.setMode("loop", true);
            WMPLib.IWMPPlaylistArray playlists = 
                Player.playlistCollection.getAll();
            for(int i = 0; i < playlists.count; i++)
            {
                if (playlists.Item(i).name == "vids")
                {
                    Player.currentPlaylist = playlists.Item(i);
                }
            }
            Player.Ctlcontrols.play();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            uint rpm;
            string s;
            try
            {
                string lastRPM = "0";
                if (!Sim)
                {
                    s = SP.ReadExisting();
                    string[] tokens = s.Split();
                    lastRPM = tokens[tokens.Length - 3];
                    rpm = uint.Parse(lastRPM);
                }
                else
                {
                    rpm = 100;
                }
            }
            catch(Exception excpt)
            {
                rpm = 0;
                excpt.ToString(); // silence 'unused variable' warning
            }
            
            Debug.WriteLine(Player.status);
            if (0 < rpm)
            {
                if (Player.status == "Paused")
                {
                    Player.Ctlcontrols.play();
                }
            }
            else
            {
                if (Player.status.Contains("Playing"))
                {
                    Player.Ctlcontrols.pause();
                }
            }
        }

        private void Player_KeyPressEvent(object sender, AxWMPLib._WMPOCXEvents_KeyPressEvent e)
        {
        }
    }
}
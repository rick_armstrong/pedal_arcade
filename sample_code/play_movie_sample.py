# BSD 3-Clause License
#
# Copyright (c) 2014, Rick Armstrong
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import wx
import wx.media
import pdb
 
class MyFrame(wx.Frame): 
    def __init__(self,parent,title): 
        wx.Frame.__init__(self, parent, -1, title, pos=(150,150), size=(640, 480),
                          style=wx.MAXIMIZE_BOX|wx.SYSTEM_MENU|wx.CAPTION|wx.CLOSE_BOX|wx.CLIP_CHILDREN)
        self.Bind(wx.EVT_CLOSE,self.OnCloseWindow)
        self.Bind(wx.EVT_IDLE, self.OnIdle)
        panel = wx.Panel(self,size=(350,200)) 
        self.video = wx.media.MediaCtrl(panel, -1, fileName=r"C:\code\pedal_arcade\sample_code\test.avi",
                                   pos=wx.Point(100,50),size=wx.Size(320,240))
 
        #self.video.ShowPlayerControls(flags = wx.media.MEDIACTRLPLAYERCONTROLS_STEP)
        #self.video.ShowPlayerControls(flags = wx.media.MEDIACTRLPLAYERCONTROLS_DEFAULT)
 
    def OnCloseWindow(self,event): 
        self.Destroy()
        
    def OnIdle(self, event):
        self.video.Play()
        
class MyApp(wx.App): 
    def OnInit(self): 
        frame = MyFrame(None,'Form1')
        frame.ShowFullScreen(True, 0)
        #frame.Show(True)
        return True
    
app=MyApp(redirect=True)
app.MainLoop()